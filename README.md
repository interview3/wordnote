# WordNote is designed for **Huawei Honor Cup 2020-2021 competitions.**
## Android, Java + Kotlin
### Used libraries: 
- https://github.com/irshuLx/Android-WYSIWYG-Editor
- https://github.com/QuadFlask/colorpicker
- https://github.com/hdodenhof/CircleImageView
- DateParser (author's library)
<p align="center">
<div align="center">
    <h1> Youtube Presentation: </h1>
      <a href="https://www.youtube.com/watch?v=rDJz7HMdjpE">
     <img 
      src="https://sun9-11.userapi.com/impg/AKT2FtGN3GBQ62AtsTRQm1fE-t1pDnjJjP6sdg/1R1Rd0XccP0.jpg?size=1604x897&quality=96&sign=8f90da32467da4eefd779adc7b62b0bc&type=album" 
      alt="WordNote Video" 
      style="width:100%;">
      </a> <br>

<img src="https://sun9-48.userapi.com/J_zgrtFAVJApd31hDeVCCgKlJ1sq0blPeZXUvg/xMlEH_HqYek.jpg" width="300" />
<img src="https://sun9-9.userapi.com/bl4gn_GF4ClfATy6GIMSyc0LqN5IHgMxIX4vwQ/hkLpdoskFFM.jpg" width="300" />
<img src="https://sun9-26.userapi.com/zPM9xSKcdGxlfUkfiU4nWKAMCGKgKu3Y8hUE5w/-oFVRlb9_Qo.jpg" width="300" />
<img src="https://sun9-69.userapi.com/YNrHThhWwH7swT7i8N5biBnjO-2Kf2g0AbBwag/o47gJfx2Boo.jpg" width="300" />
    </div>

</p>
